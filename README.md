<p align="center">
    <a target="_blank" href="https://gitee.com/x223222981/VarSpeedServo/repository/archive/master.zip"><img src="https://img.shields.io/badge/downloads-25K-blue"></a>
    <a target="_blank" href='https://gitee.com/x223222981/VarSpeedServo'><img src="https://gitee.com/x223222981/VarSpeedServo/badge/star.svg?theme=blue" /></a>
    <a target="_blank" href="LICENSE"><img src="https://img.shields.io/:license-GUN-blue.svg"></a>
    <a target="_blank" href="https://jq.qq.com/?_wv=1027&k=GEcm5Js4"><img src="https://img.shields.io/badge/Join-QQGroup-blue"></a>    
</p>


<h1 align="center">VarSpeedServo- Arduino库</h1>

>目录  
>=================  
>  
>   * [说明](#说明)  
>         * [示例1](#示例1)  
>         * [示例2](#示例2)  
>   * [库函数](#库函数)  
>   * [安装](#安装)  
>  



# 说明

这是一个用于舵机控制的`Arduino`库，[Fork源地址](https://github.com/netlabtoolkit/VarSpeedServo)，允许使用最多8个舵机异步运行(因为它使用中断)。此外，您可以自定义移动的速度，可以选择等待(block)直到伺服移动完成，并创建异步运行的移动序列。

此代码是对标准`Arduino Servo.h`库的改编，该库最初由`Korman`改编，并发布在[Arduino论坛](http://forum.arduino.cc/index.php?topic=61586.0)上，添加了更改舵机速度功能。随后[Philip van Allen](https://github.com/pvanallen)进行了`1.0 +`更新，增加了等待移动完成的功能。

* 支持最多8个舵机
* 支持所有舵机同步、异步运行
* 支持自定义舵机速度
* `write()`函数发起一个移动指令后，可以选择等待移动完成后再返回
* 支持发送一系列动作(每个动作都有位置和速度)

### 示例1  
一个舵机移动，等待第一个移动完成，然后执行另一个舵机移动


```c++

#include <VarSpeedServo.h> 
 
VarSpeedServo myservo;    // 创建一个舵机对象myservo来控制舵机
 
void setup() {
  myservo.attach(9);  // 将9号引脚连接到舵机对象myservo
} 
 
void loop() {
  myservo.write(180, 30, true);        // 移动到180度，使用30的速度，等待移动完成
  myservo.write(0, 30, true);        // 移动到0度，使用30的速度，等待移动完成
}
```


### 示例2  
两个伺服同时以不同的速度移动，等待双方完成后再做另一个动作


```c++
#include <VarSpeedServo.h> 

// 创建舵机对象
VarSpeedServo myservo1;
VarSpeedServo myservo2;
 
void setup() {
  myservo1.attach(9);
  myservo2.attach(8);
} 
 
void loop() {
  
  int LEF = 0;
  int RIG = 180;
  
  int SPEED1 = 160;
  int SPEED2 = 100;
  
  myservo1.write(LEF, SPEED1);     
  myservo2.write(LEF, SPEED2);
  myservo1.wait(); // 等待myservo1移动完成
  myservo2.wait();  // 等待myservo2移动完成
    
  myservo1.write(RIG, SPEED1);     
  myservo1.wait(); //等待myservo1移动完成
  
  myservo1.write(LEF, SPEED1); 
  myservo2.write(RIG, SPEED2);  
  myservo1.wait();
  myservo2.wait();    
        
  myservo1.write(RIG, SPEED1);     
  myservo1.wait();
      
  delay(1000);
  
}

```


其他示例包含在发行版中，可以从`Arduino示例`部分进行查看。

# 库函数

- `attach(pin ) `  // 告知Arduino舵机的数据线连接在哪一个引脚上
- `attach(pin, min, max  ) ` //设置舵机对象连接引脚及其最小和最大脉宽值(以微秒为单位) ` 
  `
- `write(value) `     // 设置舵机移动目标角度值
- `write(value, speed) ` // 设置舵机运行速度并移动新位置，0=全速，1~255由慢到快
- `write(value, speed, wait) ` // wait是一个布尔值，如果为真，则会导致函数调用block ，直到移动完成
- `writeMicroseconds() ` // 设置舵机脉冲宽度(以微秒为单位)
- `read() `      // 获取当前舵机角度值
- `readMicroseconds() `  //读取当前角度对应的脉宽
- `attached() `  // 检查某一个舵机对象是否连接在开发板引脚上
- `detach() `    // 将舵机对象与Arduino开发板断开连接
  
- `slowmove(value, speed) ` // 与write(value, speed)函数功能一样，为了与Korman的版本兼容而保留
  
- `stop() ` // 将舵机停止在当前位置
  
- `sequencePlay(sequence, sequencePositions) ` // 从位置0开始运行动作组序列
- `sequencePlay(sequence, sequencePositions, loop, startPosition) ` // 运行动作组序列，loop：循环是否启用（布尔值），startPosition：从指定位置开始
- `sequenceStop() ` // 在当前位置停止运行动作组序列
- `wait() ` // 等待舵机运行完成
- `isMoving() `  // 检查舵机当前是否在运动状态



# 安装


参考[太极创客>>为Arduino IDE安装添加库](http://www.taichi-maker.com/homepage/reference-index/arduino-library-index/install-arduino-library/)

